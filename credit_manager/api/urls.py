from django.conf.urls import include
from django.urls import path
from .views import HealthCheck

urlpatterns = [
    path('health/', HealthCheck.as_view(), name='health'),
    path('v1/', include(('api.v1.urls', 'api.v1'), namespace='v1')),
]
