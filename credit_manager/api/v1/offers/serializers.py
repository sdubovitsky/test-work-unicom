from rest_framework import serializers
from offer.models import Offer


class OfferSerializer(serializers.ModelSerializer):
    credit_org = serializers.IntegerField(source='credit_org_id')

    class Meta:
        model = Offer
        fields = '__all__'
