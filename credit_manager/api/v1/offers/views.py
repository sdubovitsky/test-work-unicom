from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from offer.models import Offer
from .serializers import OfferSerializer


class OfferViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer
    filter_fields = ['credit_org__id', 'name', 'min_score', 'max_score', 'offer_type']

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        queryset = super(OfferViewSet, self).get_queryset()

        if user.user_type == user.CREDIT_ORG:
            raise PermissionDenied()

        return queryset
