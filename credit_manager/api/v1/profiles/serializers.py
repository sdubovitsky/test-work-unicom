from rest_framework import serializers
from profile.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    partner = serializers.IntegerField(source='partner_id')

    class Meta:
        model = Profile
        fields = '__all__'


class ProfileSerializerPartnerCreate(serializers.ModelSerializer):

    class Meta:
        model = Profile
        exclude = ['partner']

    def create(self, validated_data):
        user = self.context['request'].user
        if user.user_type == user.PARTNER:
            validated_data['partner_id'] = user.id
        obj = super().create(validated_data)
        return obj
