from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from profile.models import Profile
from .serializers import ProfileSerializer, ProfileSerializerPartnerCreate


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    filter_fields = ['partner__id', 'score', 'birthday', 'phone', 'passport']

    def get_queryset(self):
        user = self.request.user
        queryset = super(ProfileViewSet, self).get_queryset()

        if user.user_type == user.CREDIT_ORG:
            raise PermissionDenied()

        if user.is_superuser:
            return queryset

        queryset = queryset.filter(partner=user)

        return queryset

    def get_serializer_class(self):
        user = self.request.user
        if user.user_type == user.PARTNER:
            return ProfileSerializerPartnerCreate
        return ProfileSerializer

