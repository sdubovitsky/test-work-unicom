from rest_framework import serializers
from query.models import Query


class QuerySerializer(serializers.ModelSerializer):
    profile = serializers.IntegerField(source='profile_id')
    offer = serializers.IntegerField(source='offer_id')

    class Meta:
        model = Query
        fields = '__all__'


class QuerySerializerUpdateCreditOrg(QuerySerializer):

    class Meta:
        model = Query
        fields = ['status']
