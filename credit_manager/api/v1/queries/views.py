from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions

from query.models import Query
from .serializers import QuerySerializer,  QuerySerializerUpdateCreditOrg


class QueryViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, DjangoModelPermissions)
    queryset = Query.objects.all()
    serializer_class = QuerySerializer
    filter_fields = ['offer__id', 'profile__id', 'status', 'created', 'sending_date']

    def get_queryset(self):
        user = self.request.user
        queryset = super(QueryViewSet, self).get_queryset()

        if user.is_superuser:
            return queryset

        if user.user_type == user.CREDIT_ORG:
            queryset = queryset.filter(offer__credit_org=user)

        if user.user_type == user.PARTNER:
            queryset = queryset.filter(profile__partner=user)

        return queryset

    def get_serializer_class(self):
        user = self.request.user
        if user.user_type == user.CREDIT_ORG and self.action == 'update':
            return QuerySerializerUpdateCreditOrg
        return QuerySerializer
