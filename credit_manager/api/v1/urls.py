from rest_framework.routers import DefaultRouter
from .profiles import views as profiles_views
from .offers import views as offers_views
from .queries import views as queries_views


router = DefaultRouter()

router.register('profiles', profiles_views.ProfileViewSet, basename='profiles')
router.register('offers', offers_views.OfferViewSet, basename='offers')
router.register('queries', queries_views.QueryViewSet, basename='queries')

urlpatterns = router.urls
