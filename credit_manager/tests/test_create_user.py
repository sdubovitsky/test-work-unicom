from django.test import TestCase

from core.models import User
from .helpers import create_user


class TestCreateUserInGroup(TestCase):

    fixtures = ['initial_data']

    def test_create_user_group(self):

        partner = create_user(User.PARTNER, 'some_partner')
        groups = partner.groups.all()
        self.assertEqual(len(groups), 1)
        self.assertEqual(groups[0].name, User.PARTNER)

        credit_org = create_user(User.CREDIT_ORG, 'some_credit_org')
        groups = credit_org.groups.all()
        self.assertEqual(len(groups), 1)
        self.assertEqual(groups[0].name, User.CREDIT_ORG)

        su = create_user(User.CREDIT_ORG, 'some_su', True)
        groups = su.groups.all()
        self.assertEqual(len(groups), 0)
