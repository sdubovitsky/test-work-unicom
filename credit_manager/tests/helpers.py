from typing import Union

from core.models import User
from offer.models import Offer
from query.models import Query


def create_user(user_type: Union[User.PARTNER, User.CREDIT_ORG], prefix: str, is_superuser: bool = False) -> User:
    data = {
        'organization': f'{prefix}_org',
        'username': f'{prefix}_user',
        'password': f'{prefix}_pass',
        'user_type': user_type,
        'is_superuser': is_superuser
    }
    return User.objects.create(**data)


def get_profile_data(partner_id: int, prefix: str) -> dict:
    return {
        "last_name": prefix,
        "first_name": prefix,
        "patronymic": prefix,
        "birthday": '1970-01-01',
        "phone": "12345679",
        "passport": "987654321",
        "score": 100,
        "partner": partner_id
    }


def get_offer_data(credit_org_id: int, prefix: str) -> dict:
    return {
        "start_rotation_date": "2018-01-01T00:00:00",
        "end_rotation_date": "2019-01-01T00:00:00",
        "name": prefix,
        "offer_type": Offer.AUTO,
        "min_score": 100,
        "max_score": 1000,
        "credit_org": credit_org_id
    }


def get_query_data(profile_id: int,
                   offer_id: int,
                   status: Union[Query.NEW, Query.APPROVED] = Query.NEW,
                   created: str = "2018-01-01T00:00:00") -> dict:
    return {
        "created": created,
        "sending_date": "2018-01-01T00:00:00",
        "status": status,
        "profile": profile_id,
        "offer": offer_id
    }
