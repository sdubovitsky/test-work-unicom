from django.urls import reverse
from rest_framework.test import APITestCase

from core.models import User
from query.models import Query
from profile.models import Profile
from .helpers import get_profile_data, get_offer_data, get_query_data


class TestApiPermissions(APITestCase):

    fixtures = ['initial_data.json', 'users.json', 'profiles.json', 'offers.json', 'queries.json']

    def _check_status_code(self, url: str, method: str, status_code: int, data: dict = None) -> None:
        response = getattr(self.client, method)(url, data)
        self.assertEqual(response.status_code, status_code)

    def test_health(self):
        url = reverse('api:health')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data, {'health': 'OK'})

    def test_auth(self):
        for api in ['profiles', 'offers', 'queries']:
            url = reverse(f'api:v1:{api}-list')
            self._check_status_code(url, 'get', 403)

            for method in ['get', 'post', 'put', 'delete']:
                url = reverse(f'api:v1:{api}-detail', kwargs={'pk': 1})
                self._check_status_code(url, method, 403)

    def test_su_permissions(self):
        su = User.objects.filter(is_superuser=True).first()
        self.client.force_login(su)

        # create
        url = reverse('api:v1:profiles-list')
        self._check_status_code(url, 'post', 201, get_profile_data(1, 'John Week'))

        url = reverse('api:v1:offers-list')
        self._check_status_code(url, 'post', 201, get_offer_data(1, 'offer'))

        url = reverse('api:v1:queries-list')
        self._check_status_code(url, 'post', 201, get_query_data(1, 1))

        # put
        url = reverse('api:v1:profiles-detail', kwargs={'pk': 1})
        self._check_status_code(url, 'put', 200, get_profile_data(1, 'John Week'))

        url = reverse('api:v1:offers-detail', kwargs={'pk': 1})
        self._check_status_code(url, 'put', 200, get_offer_data(1, 'offer1123'))

        url = reverse('api:v1:queries-detail', kwargs={'pk': 1})
        self._check_status_code(url, 'put', 200, get_query_data(1, 2))

        # get, delete
        for api in ['queries', 'offers', 'profiles']:
            url = reverse(f'api:v1:{api}-list')
            resp = self.client.get(url)
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(len(resp.data), 3)

            url = reverse(f'api:v1:{api}-detail', kwargs={'pk': 1})
            resp = self.client.get(url)
            self.assertEqual(resp.status_code, 200)

            resp = self.client.delete(url)
            self.assertEqual(resp.status_code, 204)

        # delete cascade
        url = reverse(f'api:v1:profiles-detail', kwargs={'pk': 2})
        resp = self.client.delete(url)
        self.assertEqual(resp.status_code, 204)

        url = reverse(f'api:v1:queries-list')
        resp = self.client.get(url)
        self.assertEqual(len(resp.data), 0)

    def test_credit_org_permissions(self):
        credit_org = User.objects.filter(user_type=User.CREDIT_ORG).first()
        self.client.force_login(credit_org)

        # post and get not allowed
        for api in ['profiles', 'offers']:
            url = reverse(f'api:v1:{api}-list')
            self._check_status_code(url, 'get', 403)
            self._check_status_code(url, 'post', 403)

        for m in ['get', 'post', 'put', 'delete']:

            # profiles
            url = reverse('api:v1:profiles-detail', kwargs={'pk': 1})
            self._check_status_code(url, m, 403)

            # offers
            url = reverse('api:v1:offers-detail', kwargs={'pk': 1})
            self._check_status_code(url, m, 403)

        # get queries list only for self
        url = reverse('api:v1:queries-list')
        resp = self.client.get(url)
        queries = resp.data
        self.assertEqual(len(queries), 1)

        # post not allowed for query
        self._check_status_code(url, 'post', 403, get_query_data(1, 1))

        # disabled access for query other org
        url = reverse('api:v1:queries-detail', kwargs={'pk': 2})
        self._check_status_code(url, 'get', 404)

        # put allowed for query. allow update only status field
        url = reverse('api:v1:queries-detail', kwargs={'pk': queries[0]['id']})
        new_created = "1970-01-01T00:00:00"
        self._check_status_code(url, 'put', 200, get_query_data(1, 1, status=Query.APPROVED, created=new_created))

        url = reverse('api:v1:queries-list')
        resp = self.client.get(url)
        updated_queries = resp.data
        self.assertEqual(updated_queries[0]['status'], Query.APPROVED)
        self.assertEqual(updated_queries[0]['created'], queries[0]['created'])

    def test_partner_permissions_profiles(self):
        partner = User.objects.filter(user_type=User.PARTNER).first()
        self.client.force_login(partner)

        # get profiles list only for self
        url = reverse('api:v1:profiles-list')
        resp = self.client.get(url)
        profiles = resp.data
        self.assertEqual(len(profiles), 1)

        # create allowed
        new_profile = get_profile_data(partner.id + 1, 'some_profile')
        resp = self.client.post(url, data=new_profile)
        self.assertEqual(resp.status_code, 201)
        new_profile = resp.data
        # check self partner
        new_profile_partner = Profile.objects.get(pk=new_profile['id']).partner
        self.assertEqual(partner.id, new_profile_partner.id)

        # update not allowed
        url = reverse('api:v1:profiles-detail', kwargs={'pk': new_profile['id']})
        update_profile = get_profile_data(partner.id, 'some_profile_1')
        resp = self.client.put(url, data=update_profile)
        self.assertEqual(resp.status_code, 403)

        # delete not allowed
        resp = self.client.delete(url)
        self.assertEqual(resp.status_code, 403)

        # get not allowed for other partner
        url = reverse('api:v1:profiles-detail', kwargs={'pk': 2})
        update_profile = get_profile_data(partner.id, 'some_profile_2')
        resp = self.client.put(url, data=update_profile)
        self.assertEqual(resp.status_code, 403)

    def test_partner_permissions_offers(self):
        partner = User.objects.filter(user_type=User.PARTNER).first()
        self.client.force_login(partner)

        # get profiles list only for self
        url = reverse('api:v1:offers-list')
        resp = self.client.get(url)
        offers = resp.data
        self.assertEqual(len(offers), 2)

        # post not allowed
        self._check_status_code(url, 'post', 403, get_offer_data(1, 'some_offer'))

        # delete, put not allowed
        url = reverse('api:v1:offers-detail', kwargs={'pk': 1})
        self._check_status_code(url, 'delete', 403)
        self._check_status_code(url, 'put', 403, get_offer_data(1, 'some_offer'))

    def test_partner_permission_queries(self):
        partner = User.objects.filter(user_type=User.PARTNER).first()
        self.client.force_login(partner)

        # get queries list only for self
        url = reverse('api:v1:queries-list')
        resp = self.client.get(url)
        offers = resp.data
        self.assertEqual(len(offers), 1)

        # post allowed
        resp = self.client.post(url, get_query_data(1, 1))
        self.assertEqual(resp.status_code, 201)
        new_query = resp.data

        # update not allowed
        url = reverse('api:v1:queries-detail', kwargs={'pk': new_query['id']})
        resp = self.client.put(url, new_query)
        self.assertEqual(resp.status_code, 403)

        # delete not allowed
        resp = self.client.delete(url)
        self.assertEqual(resp.status_code, 403)
