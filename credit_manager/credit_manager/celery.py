from celery import Celery
from kombu import Exchange, Queue

from django.conf import settings

app = Celery('credit_manager', broker=settings.REDIS_BROKER_URL, include=['credit_manager.tasks'])

app.conf.task_queues = (
    Queue('default_queue', Exchange('default_exchange'), routing_key='default_key'),
)

app.conf.task_default_queue = 'default_queue'
app.conf.task_default_exchange = 'default_exchange'
app.conf.task_default_routing_key = 'default_key'

app.conf.task_routes = {
    '*': {'queue': 'default_queue', 'routing_key': 'default_key'},
}

app.conf.beat_schedule = {
    'some_periodic_task': {
        'task': 'credit_manager.tasks.SomeTask',
        'schedule': 10
    }
}
