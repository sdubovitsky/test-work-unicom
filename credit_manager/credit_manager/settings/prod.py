from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    'localhost',
    '89.223.24.33'
]


STATIC_ROOT = '/unicom/files/static/'
