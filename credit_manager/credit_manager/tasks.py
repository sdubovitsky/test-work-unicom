import logging
import time
from .celery import app

logger = logging.getLogger('worker')


class SomeTask(app.Task):

    def __init__(self):
        self.max_retries = 3
        self.time_limit = 30
        self.default_retry_delay = 10

    def _run(self):
        logging.debug(f'Run task: {self}')
        time.sleep(10)

    def run(self):
        try:
            return self._run()
        except Exception as e:
            logger.exception('Got unhandled exception')
            self.retry()


some_task = app.register_task(SomeTask())
