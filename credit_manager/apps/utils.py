from django.db import models
from django.contrib import admin


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    update = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')

    class Meta:
        abstract = True


def create_admin_class(model_class: models.Model) -> admin.options.forms.MediaDefiningClass:
    all_fields = model_class._meta.fields

    readonly_fields = [f.name for f in all_fields if not f.editable]
    date_fields = [f.name for f in all_fields if type(f) in (models.DateTimeField, models.DateField)]
    fk_fields = [f.name for f in all_fields if type(f) == models.ForeignKey]

    admin_class = type('ModelAdmin', (admin.ModelAdmin,), {
        'list_display': [f.name for f in all_fields],
        'readonly_fields': [f.name for f in all_fields if not f.editable],
        'search_fields': [f for f in all_fields if f not in fk_fields],
        'list_filter': tuple(set(fk_fields + date_fields + readonly_fields)),
        'raw_id_fields': fk_fields
    })

    return admin_class


def admin_register(model_class: models.Model):
    admin.site.register(model_class, create_admin_class(model_class))
