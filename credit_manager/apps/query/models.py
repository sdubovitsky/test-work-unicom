from django.db import models

from profile.models import Profile
from offer.models import Offer


class Query(models.Model):

    NEW = 'new'
    SENT = 'sent'
    RECEIVED = 'received'
    APPROVED = 'approved'
    DENIED = 'denied'
    ISSUED = 'issued'

    STATUS_CHOICES = [
        (NEW, 'Новая'),
        (SENT, 'Отправлена'),
        (RECEIVED, 'Получена'),
        (APPROVED, 'Одобрена'),
        (ISSUED, 'Выдана'),
    ]

    created = models.DateTimeField(verbose_name='Дата создания')
    sending_date = models.DateTimeField(verbose_name='Дата отправки')
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, verbose_name='Статус')
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name='Анкета клиента')
    offer = models.ForeignKey(Offer, on_delete=models.CASCADE, verbose_name='Предложение')

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'

    def __str__(self):
        return self.status
