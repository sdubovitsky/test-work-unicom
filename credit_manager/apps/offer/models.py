from django.core.exceptions import ValidationError
from django.db import models

from utils import BaseModel
from core.models import User


class Offer(BaseModel):

    CONSUMER = 'consumer'
    MORTGAGE = 'mortgage'
    AUTO = 'auto'

    OFFER_TYPE_CHOICES = [
        (CONSUMER, 'Потребительский'),
        (MORTGAGE, 'Ипотека'),
        (AUTO, 'Автокредит'),
    ]

    start_rotation_date = models.DateTimeField(verbose_name='Дата начала ротации')
    end_rotation_date = models.DateTimeField(verbose_name='Дата окончания ротации')
    name = models.CharField(max_length=50, verbose_name='Название')
    offer_type = models.CharField(max_length=8, choices=OFFER_TYPE_CHOICES, verbose_name='Тип')
    min_score = models.PositiveIntegerField(verbose_name='Минимальный скоринговый бал')
    max_score = models.PositiveIntegerField(verbose_name='максимальный скоринговый бал')
    credit_org = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Кредитная организация')

    class Meta:
        verbose_name = 'Предложение'
        verbose_name_plural = 'Предложения'

    def __str__(self):
        return self.name

    def clean(self):
        if self.credit_org.user_type != self.credit_org.CREDIT_ORG:
            raise ValidationError({'partner': 'Неверный тип пользователя'})
