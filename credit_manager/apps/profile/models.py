from django.core.exceptions import ValidationError
from django.db import models

from utils import BaseModel
from core.models import User


class Profile(BaseModel):
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    patronymic = models.CharField(max_length=100, verbose_name='Отчество')
    birthday = models.DateField(verbose_name='Дата рождения')
    phone = models.CharField(max_length=20, verbose_name='Телефон')
    passport = models.CharField(max_length=20, verbose_name='Паспорт')
    score = models.PositiveIntegerField(verbose_name='Скоринговый бал')
    partner = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Партнер')

    class Meta:
        verbose_name = 'Анкета'
        verbose_name_plural = 'Анкеты'

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.patronymic}'

    def clean(self):
        if self.partner.user_type != self.partner.PARTNER:
            raise ValidationError({'partner': 'Неверный тип пользователя'})
