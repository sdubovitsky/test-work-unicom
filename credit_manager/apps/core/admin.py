from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


class CustomUserAdmin(UserAdmin):
    model = User
    list_display = ('username', 'email',  'organization', 'user_type', 'is_superuser')
    list_filter = ('user_type', 'organization')

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email',  'organization', 'user_type', 'password1', 'password2',)
        }),

    )

    fieldsets = ((None, {'fields': ('organization', 'user_type')}),) + UserAdmin.fieldsets


admin.site.register(User, CustomUserAdmin)
