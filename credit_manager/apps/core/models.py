import logging
from django.contrib.auth.models import AbstractUser, Group
from django.db import models

logger = logging.getLogger(__name__)


class User(AbstractUser):

    PARTNER = 'partner'
    CREDIT_ORG = 'credit_org'

    USER_TYPE_CHOICES = [
        (PARTNER, 'Партнер'),
        (CREDIT_ORG, 'Кредитная организация')
    ]

    organization = models.CharField(max_length=160, verbose_name='Название организации')
    user_type = models.CharField(max_length=10, blank=True, verbose_name='Тип пользователя', choices=USER_TYPE_CHOICES)

    def __str__(self):
        return self.username


def add_user_to_group(sender, instance: User, created: bool, **kwargs):
    logger.debug(f'Save user: {instance}')
    try:
        if created and not instance.is_superuser:
            logger.debug(f'Add user: {instance} to group')
            group = Group.objects.get(name=instance.user_type)
            instance.groups.add(group)
            instance.save()
            logger.debug(f'User: {instance} added to group: {group}')
    except Group.DoesNotExist:
        logger.info(f'Group not found for user')


models.signals.post_save.connect(add_user_to_group, sender=User)
